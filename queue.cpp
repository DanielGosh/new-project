#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	q->_elements = new int[size];
	q->_first = 0;
	q->_last = size - 1;
	q->_size = size;
	q->_index = 0;
	q->_status = 100;
}

void cleanQueue(queue* q)
{
	delete[] q->_elements;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (isFull(q))
	{
		return;
	}
	else
	{
		q->_status = 101;
		q->_elements[q->_index] = newValue;
		if (q->_index < q->_size - 1) {
			q->_index++;
		}
		else
		{
			q->_status = 111;
		}
	}
}

int dequeue(queue* q)// return element in top of queue, or -1 if empty
{
	int val = 0;
	if (isEmpty(q))
	{
		return -1;
	}
	else
	{
		val = q->_elements[q->_first];
		if (q->_first < q->_size - 1) {
			q->_first++;
		}
	}
	return val;
}

bool isEmpty(queue* q)
{
	if (q->_elements[q->_status] == 100)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool isFull(queue* q)
{
	if (q->_elements[q->_status] == 111)
	{
		return true;
	}
	else
	{
		return false;
	}
}