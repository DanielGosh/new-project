#pragma once

#ifndef QUEUE_H
#define QUEUE_H

typedef struct numNode
{
	int num;
	struct numNode* next;
}numNode;

void cleanStack(numNode* head);
numNode* add(numNode* head, int val);
numNode* take(numNode* head);
#endif