#pragma once
#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	int* _elements;
	int _first;
	int _last;
	int _size;
	int _index;
	int _status;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

bool isFull(queue* q);
bool isEmpty(queue* q);
#endif /* QUEUE_H */